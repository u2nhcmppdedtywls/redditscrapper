#[cfg(test)]
mod test {
    use reddit_crawler::subreddit::scan::scan_pages;
    use std::time::SystemTime;

    extern crate rusqlite;
    use rayon::prelude::*;
    use reddit_crawler::subreddit::sql;
    use reddit_crawler::subreddit::sql::{get_sid, insert_subreddit};
    use rusqlite::NO_PARAMS;
    use rusqlite::{Connection, Result};
    use std::fs::remove_file;

    #[test]
    fn new_subreddits() {
        remove_file("db.sqlite3");

        let start = SystemTime::now();
        let to_scan: i64 = 1;
        let url = vec![
            "/r/nsfw",
            "/r/tentai",
            "/r/overwatch_porn",
            "/r/hentaibondage",
            "/r/ahegao",
            "/r/TeenTitansPorn",
        ];

        let mut conn = sql::get_conn().unwrap();
        sql::create(&mut conn).unwrap();
        conn.close().unwrap();

        let f: Vec<u8> = url
            .par_iter()
            .map(|p| {
                let mut connx = sql::get_conn().unwrap();
                insert_subreddit(&mut connx, p).unwrap();
                let sid = get_sid(&mut connx, p).unwrap().to_string();
                scan_pages(p, None, to_scan, &mut connx, &sid);
                connx.close().unwrap();
                0
            })
            .collect();

        let elapsed = start.elapsed().unwrap();
        let scanned = to_scan * url.len() as i64;
        println!("Scanned {:?} pages in {:?} sec", scanned, elapsed);
        println!("{:?} pages/sec", scanned as f64 / elapsed.as_secs() as f64);
    }
}
