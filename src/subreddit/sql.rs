extern crate rusqlite;
use rusqlite::NO_PARAMS;
use rusqlite::{Connection, Result};

pub fn get_conn() -> Result<Connection> {
    let conn = Connection::open("db.sqlite3")?;
    //conn.execute("PRAGMA journal_mode = WAL", NO_PARAMS)?;
    conn.execute("PRAGMA synchronous = OFF", NO_PARAMS)?;

    Ok(conn)
}

pub fn create(conn: &mut Connection) -> Result<()> {
    let tx = conn.transaction()?;
    tx.execute(
        "CREATE TABLE \"subreddit\" (
	            \"id\"	INTEGER PRIMARY KEY AUTOINCREMENT,
	            \"url\"	TEXT
            )",
        NO_PARAMS,
    )?;

    tx.execute(
        "CREATE TABLE \"post\" (
	            \"id\"	INTEGER PRIMARY KEY AUTOINCREMENT,
	            \"subreddit_id\"	INTEGER,
	            \"title\"	TEXT,
	            \"permalink\"	TEXT,
	            \"img_url\"	TEXT,
	            \"author\"	TEXT,
	            FOREIGN KEY(\"subreddit_id\") REFERENCES \"subreddit\"(\"id\")
            )",
        NO_PARAMS,
    )?;

    tx.commit()?;
    Ok(())
}

pub fn insert_subreddit(conn: &mut Connection, url: &str) -> Result<()> {
    conn.execute("INSERT INTO subreddit (id, url) VALUES (NULL, ?1)", &[url])?;

    Ok(())
}

pub fn get_sid(conn: &mut Connection, url: &str) -> Result<i64> {
    let mut stmt = conn.prepare("SELECT id FROM subreddit WHERE url = ?1 LIMIT 1")?;

    let repo = stmt.query_map(&[url], |row| Ok(row.get(0)?))?;

    match repo.last() {
        Some(x) => x,
        None => Err(rusqlite::Error::InvalidQuery),
    }
}

pub fn insert_page(
    conn: &mut Connection,
    posts: &[super::subreddit_model::Child],
    sid: &str,
) -> Result<()> {
    let tx = conn.transaction()?;

    for post in posts {
        let pd = post.data.as_ref().unwrap();
        let values = [
            sid,
            pd.title.as_ref().unwrap(),
            pd.permalink.as_ref().unwrap(),
            pd.url.as_ref().unwrap(),
            pd.author.as_ref().unwrap(),
        ];
        println!("{:?}", &values);
        tx.execute(
            "INSERT INTO post\
             (id, subreddit_id, title, permalink, img_url, author)\
             VALUES (NULL, ?1, ?2, ?3, ?4, ?5);\
             ",
            &values,
        )?;
    }

    tx.commit()?;

    Ok(())
}
