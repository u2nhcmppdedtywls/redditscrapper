extern crate custom_error;
use crate::subreddit::subreddit_model::Subreddit;
use custom_error::custom_error;
use reqwest::header::USER_AGENT;
use reqwest::StatusCode;
use rusqlite::Connection;

custom_error! {pub NewSubredditError
    GenericError = "Generic Error!",
    HTTPError{source: reqwest::Error} = "Http Error",
    SerdeError{source: serde_json::Error} = "Serde Error",
    IOError{source: std::io::Error} = "IO Error",
}

/// Adds a new subreddit to the database
/// * `url_path` : Subreddit path. Ex: /r/example
pub fn scan_pages(
    url_path: &str,
    page_id: Option<&str>,
    mut to_scan: i64,
    conn: &mut Connection,
    sid: &str,
) {
    if to_scan == 0 {
        return;
    }
    to_scan -= 1;

    let after = format!("?after={}", page_id.unwrap_or(""));

    let url = format!(
        "https://www.reddit.com/{}/.json{}",
        &url_path,
        if page_id.is_some() { &after } else { "" }
    );

    println!("Page: {:?}", page_id.unwrap_or(""));

    let client = reqwest::Client::new();
    let mut response = client
        .get(&url)
        .header(
            USER_AGENT,
            "User-Agent: rust:soontm.net.reddit_crawler:v0.1.0 (by /u/scarjit)",
        )
        .send()
        .unwrap();

    if response.status() != StatusCode::from_u16(200).unwrap() {
        println!(
            "Invalid HTTP status code: {:?} || {:?}",
            response.status(),
            response.headers()
        );
    }

    println!("{:?}", response.headers());

    let json: String = response.text().unwrap();

    let model: Subreddit = serde_json::from_str(&json).unwrap();

    let mdata = model.data.unwrap();

    let children = mdata.children.unwrap();

    super::sql::insert_page(conn, &children, sid).unwrap();

    scan_pages(
        url_path,
        Some(mdata.after.unwrap().as_str()),
        to_scan,
        conn,
        sid,
    );
}
