#![feature(core_intrinsics)]
#![feature(type_ascription)]
#![feature(const_fn)]
#![feature(seek_convenience)]
#![feature(proc_macro_hygiene)]

#[macro_use]
extern crate serde_derive;

pub mod subreddit;
